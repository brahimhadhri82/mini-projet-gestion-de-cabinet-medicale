package com.cabinet.medical.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cabinet.medical.model.Consultation;


public interface IConsultationDao extends JpaRepository<Consultation, String> {

	Consultation findByNumcons(int num);

}
