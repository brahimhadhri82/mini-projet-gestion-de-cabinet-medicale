package com.cabinet.medical.controller;

import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.cabinet.medical.exception.EntiteNotFoundException;
import com.cabinet.medical.exception.EntiteTransactionException;
import com.cabinet.medical.model.Antecedant;
import com.cabinet.medical.model.Consultation;
import com.cabinet.medical.model.Patient;
import com.cabinet.medical.service.ConsultationMetier;

@CrossOrigin(origins = "*")
@RestController
public class ConsultationRestController {
	ConsultationMetier consultationservice ;
	
	
	@PostMapping(value = "/consultation")
	public Consultation saveConsultation(@RequestBody Consultation c) throws EntiteTransactionException {
		return consultationservice.saveConsultation(c);
	}
	
	@RequestMapping(value = "/consultation/{num}", method = RequestMethod.GET)
	public Consultation getConsultation(@PathVariable int num)throws EntiteNotFoundException {
		return  consultationservice.getByNumcons(num);
	}
	
	
	
	
	

}
