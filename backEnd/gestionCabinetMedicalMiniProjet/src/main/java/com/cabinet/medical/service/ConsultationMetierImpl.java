package com.cabinet.medical.service;

import org.springframework.beans.factory.annotation.Autowired;

import com.cabinet.medical.exception.EntiteNotFoundException;
import com.cabinet.medical.exception.EntiteTransactionException;
import com.cabinet.medical.model.Consultation;
import com.cabinet.medical.model.Patient;
import com.cabinet.medical.repository.IConsultationDao;
import com.cabinet.medical.repository.IPatientDao;

public class ConsultationMetierImpl implements ConsultationMetier {
	
	@Autowired
	private IConsultationDao consultationdao;

	@Override
	public Consultation saveConsultation(Consultation c) throws EntiteTransactionException {
		
		return consultationdao.save(c) ;
	}

	@Override
	public Consultation getByNumcons(int num)throws EntiteNotFoundException {
		return consultationdao.findByNumcons(num) ;
	}
	

}
