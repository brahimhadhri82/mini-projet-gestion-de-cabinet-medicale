package com.cabinet.medical;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import com.cabinet.medical.exception.EntiteNotFoundException;
import com.cabinet.medical.exception.EntiteTransactionException;
import com.cabinet.medical.exception.NoDataFoundException;
import com.cabinet.medical.model.Patient;


@SpringBootApplication
public class GestionCabinetMedical1Application {

	public static void main(String[] args) throws NoDataFoundException, EntiteNotFoundException, EntiteTransactionException {
		//SpringApplication.run(GestionCabinetMedical1Application.class, args);
		ApplicationContext contexte=
				SpringApplication.run(GestionCabinetMedical1Application.class, args);
		
		/*PatientMetierImpl repositoryPatient = contexte.getBean(PatientMetierImpl.class) ;
		 System.out.println(repositoryPatient.getPatient("11111111"));
		 
		 Optional<Patient> p = repositoryPatient.getPatient("11111111") ;
		 Date d = new Date() ;*/
		 //System.out.println(" le nom de patient est : "+ p.getCin()+ "Son nom est " + p.getNomp()+" Son prénom"+ p.getPrenomp());
		
		 //Patient p1= new Patient("33333333","hadhri","kuhiuhu",d,d,"fe","prof","sfax","93701363");
		 //repositoryPatient.savePatient(p1) ;
		// repositoryPatient.deletePatient("11111111");
		 
//		 List<Patient> listPatient = (List<Patient>) repositoryPatient.listPatient(null);
//		 for (Patient patients : listPatient) {
//			 System.out.println(" son cin est : "+ patients.getCin() );
//			 
//		 }
		 
		//System.out.println(p.getPatient("11111111"));
	}

}
