package com.cabinet.medical.service;

import com.cabinet.medical.exception.EntiteNotFoundException;
import com.cabinet.medical.exception.EntiteTransactionException;
import com.cabinet.medical.model.Consultation;


public interface ConsultationMetier {
	Consultation saveConsultation(Consultation c) throws EntiteTransactionException;

	Consultation getByNumcons(int num) throws EntiteNotFoundException;

}
