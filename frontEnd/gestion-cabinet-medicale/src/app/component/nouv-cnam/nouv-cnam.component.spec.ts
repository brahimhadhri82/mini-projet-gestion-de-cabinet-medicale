import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NouvCnamComponent } from './nouv-cnam.component';

describe('NouvCnamComponent', () => {
  let component: NouvCnamComponent;
  let fixture: ComponentFixture<NouvCnamComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NouvCnamComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NouvCnamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
