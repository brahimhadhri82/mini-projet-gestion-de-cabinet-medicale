import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CNAM } from 'src/app/model/cnam';
import { CnamService } from 'src/app/service/cnam.service';
import { ToasterService } from 'src/app/service/toaster.service';

@Component({
  selector: 'app-nouv-cnam',
  templateUrl: './nouv-cnam.component.html',
  styleUrls: ['./nouv-cnam.component.css']
})
export class NouvCnamComponent implements OnInit {
  date: Date = new Date();
  datecread: Date = new Date();
  servCnam!: CnamService;
  c!: CNAM;
  toast: ToasterService;
  constructor(servCnam: CnamService, private router: Router, toast: ToasterService) {
    this.servCnam = servCnam
    this.toast = toast
  }
  ngOnInit(): void {
  }
  onSaveCnam(data: any) {
    console.log(data)
    this.c = new CNAM();
    console.log('c', this.c)

    this.servCnam.creerCnam(data).subscribe(res => {
      this.toast.showSuccess("Cnam Ajouté avce succée");
      setTimeout(() => {
        this.router.navigateByUrl('/dashboard');
      }, 1000);
    }, err => {

      this.toast.showWarning(err.message);
    })
  }
}
