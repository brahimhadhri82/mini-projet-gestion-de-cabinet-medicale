import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Comment, comments } from './comments-data';

@Component({
  selector: 'app-recent-comments',
  templateUrl: './recent-comments.component.html',
  styleUrls: ['./recent-comments.component.css']
})
export class RecentCommentsComponent implements OnInit {

  userComments: Comment[];

  constructor(private activatedroute: ActivatedRoute) {
    this.userComments = comments;

  }

  ngOnInit(): void {
  }
 /*  ngAfterViewInit() {

  } */
}
