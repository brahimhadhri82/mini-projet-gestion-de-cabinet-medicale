import { TestBed } from '@angular/core/testing';

import { CnamService } from './cnam.service';

describe('CnamService', () => {
  let service: CnamService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CnamService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
