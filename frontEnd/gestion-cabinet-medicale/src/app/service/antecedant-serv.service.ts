import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Antecedant } from '../model/antecedant';

@Injectable({
  providedIn: 'root'
})
export class AntecedantServService {

  constructor(private httpClient:HttpClient) { }
  host:string="http://localhost:5000/"
  public creerAntecedant(a:Antecedant){
    return this.httpClient.post(this.host+"antecedant",a)}
    public getDescant(cat:string){
      return this.httpClient.get<string[]>(this.host+"antecedantByDescreption/"+cat)
    }
    public getCategories(){
      return this.httpClient.get<string[]>(this.host+"antecedant")
    }
    public getAntecedants(){
      return this.httpClient.get<Antecedant[]>(this.host+"antecedant")
    }
    public getByDesc(desc:string){
      return this.httpClient.get<Antecedant>(this.host+"antecedants/"+desc)
    }
    public updateAntecedant(antecedant:Antecedant){
     
      return this.httpClient.put(this.host+"antecedant",(<object>antecedant));
    }


}
