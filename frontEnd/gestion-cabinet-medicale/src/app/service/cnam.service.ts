import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CNAM } from '../model/cnam';

@Injectable({
  providedIn: 'root'
})
export class CnamService {


  constructor(private httpClient: HttpClient) { }
  host: string = "http://localhost:5000/"
  errorMsg!: string;
  public getCnam(page: number, size: number) {

    return this.httpClient.get<CNAM[]>(this.host + "cnam" ) //+ "?page=" + page + "&size=" + size
  }
  public creerCnam(c: CNAM) {

    console.log("cnam",c);
    return this.httpClient.post(this.host + "cnam", c)
  }
  public getCnamByCin(cin: string) {
    return this.httpClient.get<CNAM>(this.host + "Cnam/" + cin)
  }
  public updateCnam(cnam: CNAM) {
    return this.httpClient.put(this.host + "Cnam", cnam)
  }
  public deleteCnam(Cnam: CNAM) {
    return this.httpClient.put(this.host + "Cnam", Cnam)
  }
}
